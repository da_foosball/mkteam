<?php

  $output=shell_exec("tail -1 /home/kristian/bordfodbold/team_predict.log");
  
  $f=explode(" ",$output);
  if (count($f)!=4) die(json_encode(array("error"=>"Error parsing team log")));

  $timetxt=$f[0]." ".$f[1];
  $date=strtotime($timetxt);
  $now=time();
  $ellapse=$now-$date;
  if ($ellapse>3000000) die(json_encode(array("error"=>"Date error: $date")));

  $players=@explode(",",trim($f[3]));
  if (count($players)!=4) die(json_encode(array("error"=>"Error parsing player names")));
  $yellow = array($players[0],$players[1]);
  $black  = array($players[2],$players[3]);

  $result=array("time" => $date, "timetxt" => $timetxt, "yellow"=>$yellow, "black"=>$black);
  print(json_encode($result));

?>
