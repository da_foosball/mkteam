<?php
require("docheader.inc");
require("kdb.inc");

$dbh=MyDatabase::connect("kageliste");

$players=$dbh->get_single_column("select player from vkampliste where date>adddate(current_date(),-90) group by player order by -count(*)");
$players[]="Ukendt 1";
$players[]="Ukendt 2";

$header=new Docheader();
$header->set_html5();

$header->set_title("SKAT Bordfodboldhold");
$header->add_css("mkteam.css");
$header->add_jquery();
$header->add_javascript("mkteam.js");

$header->display();

print("\n".
      "<div id='header'> Lav hold </div>\n".
      "<div id='teamresult' class='container'>\n".
      "  <p id='gul' class='team'><span id='gulnames'></span><br><span id='gulrank'></span>\n".
      "  <p class='space'> vs.\n".
      "  <p id='sort' class='team'><span id='sortnames'></span><br><span id='sortrank'></span>\n".
      "  <p class='space' id='expect'>\n".
      "  <p><input class='run' id='back' type='button' value='Andre hold'>\n".
      "</div>\n\n".

      "<div id='teamselect' class='container'>\n".
      "  <form action=''>\n".
      "    <input class='run' id='run_random' type='submit' value='Tilfældige'>\n".
      "    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n".
      "    <input class='run' id='run_fair' type='submit' value='Mest fair'>\n".
      "    <p>\n");
$n=0;
foreach ($players as $p) {
  printf("    <input class='player' type='button' id='%-10s value='$p'>\n",preg_replace("/[^\w]/","_",$p)."'");
  if ($n++ % 4 == 3) print("    <br>\n");
}
  print("    <input type='button' id='lastteam' value='Seneste hold'>\n");

print("  </form>\n".
      "  <p id='error'> <span></span>\n".
      "  </body>\n".
      "</div>\n".
      "</html>\n");
?>
