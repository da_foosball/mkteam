var players=[];

$(document).ready(function(){
  $('.player').click(toggleclick);
  $('#run_random').click(function(){maketeams("random");return(false);});
  $('#run_fair').click(function(){maketeams("fair");return(false);});
  $('#back').click(newteams);
  $('#lastteam').click(getLastTeams);
});

function toggleclick() {
  toggleplayer(this.id);
}

function toggleplayer(id) {
  var i=players.indexOf(id);
  $("#error span").html("");
  if (i>-1) {
    // Remove player from active list
    players.splice(i,1);
    $("#"+id).css({'background':'lightgray','color':'black','font-weight':'normal'});
  }
  else {
    // Add player to active list
    players.push(id);
    $("#"+id).css({'background':'darkgreen','color':'white', 'font-weight':'bold'});
  }
  if (players.length>4) toggleplayer(players[0]);
}

function insertTeamInfo(json) {
  if (json.error==1) {
    $("#error span").html("Fejl: "+json.message);
    return(false);
  }
  $("#gulnames").html(json.players[0].name+" / "+json.players[1].name);
  $("#sortnames").html(json.players[2].name+" / "+json.players[3].name);
  $("#gulrank").css("visibility", "visible");
  $("#sortrank").css("visibility", "visible");
  $("#expect").css("visibility", "visible");

  if (json.error == 0) {
    var expect=(json.goalsexpect/10).toFixed(1);
    $("#gulrank").html(json.players[0].rank/100+"/"+json.players[1].rank/100+" - chance for sejr: "+(json.winchance/10).toFixed(1)+"%");
    $("#sortrank").html(json.players[2].rank/100+"/"+json.players[3].rank/100+" - chance for sejr: "+(100-json.winchance/10).toFixed(1)+"%");
    $("#expect").html("Forventet resultat 11-"+expect+"<br>Potentiel rating effekt: "+json.changea/100+"/"+json.changeb/100);
  }
  else {
    console.log("Holdstyrker kan ikke beregnes");
  }
}

function insertLastTeams(json) {
  if (json.error==1) {
    $("#error span").html("Fejl: "+json.message);
    return(false);
  }
  $("#gulnames").html(json.yellow[0]+" / "+json.yellow[1]);
  $("#sortnames").html(json.black[0]+" / "+json.black[1]);
  $("#gulrank").css("visibility", "visible");
  $("#sortrank").css("visibility", "visible");
  $("#expect").css("visibility", "visible");
  $("#expect").html(json.timetxt);
}
  

function maketeams(type) {
  if (players.length<4) {
    $("#error span").html("Vælg 4 spillere for at lave hold");
    return(false);
  }
  var url="team_predict.php?gametype="+type+"&players="+players.join(",");
  $.ajax({
    dataType: 'json',
    url: url,
    success: insertTeamInfo
  });

  $("#teamselect").fadeOut(1200,faderesult);

  return(false);
}

function getLastTeams() {
  var url="last_teams.php";
  $.ajax({
    dataType: 'json',
    url: url,
    success: insertLastTeams
  });

  $("#teamselect").fadeOut(100,fastresult);

  return(false);
}

function faderesult() {
  $("#teamresult").fadeIn(500);
}
function fastresult() {
  $("#teamresult").fadeIn(200);
}

function newteams() {
  $("#teamselect").show();
  $("#teamresult").hide();

  $("#gulrank").css("visibility", "hidden");
  $("#sortrank").css("visibility", "hidden");
  $("#expect").css("visibility", "hidden");

  $("#gulnames").html("");
  $("#sortnames").html("");
  $("#gulrank").html("");
  $("#sortrank").html("");
  $("#expect").html("");

  return(false);
}
