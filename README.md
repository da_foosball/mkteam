MkTeam
======

Small website to make random/fair teams when we play foosball.

See active page at: http://www.ougar.dk/mkteam

A list of players who have played within last 90 days is retrived from the match  
database, and a simple webpage is build, where these names - as well as a few  
extra potential "guests" - are shown as buttons.

Using som simple javascript, players can then be selected and unselected.  
When the "Fair" or "Random" team-button is clicked, a php-script  
team_predict.php is called from javascript with the 4 selected players.

This script then calls (somewhat comvoluted) a python script, which fetches info  
about the players from the ranking database, and created the teams and also  
computes the winchance of each team as well as an expected final score and  
potential effect on the player rankings. This script returns a json object,  
which is then recieved by the original calling javascript code, which updates  
the DOM with team information.

Example of returned json data:  
http://www.ougar.dk/mkteam/team_predict.php??gametype=random&players=Peter,Anna,Kristian,Manja

