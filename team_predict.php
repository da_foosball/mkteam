<?php

# On error, print json-object with error details
function error($errorlevel, $errormessage) {
  print(json_encode(array("error"=>$errorlevel,"message"=>$errormessage)));
  die();
}

header('Content-Type: application/json');

# Get variable players is required
if (!array_key_exists("players",$_GET))
  error(1,"No players supplied");

# Get gametype - default to random teams (no flag)
$gametype=@$_GET["gametype"];
$flag="";
if     ($gametype=="fair")    $flag="-f";
elseif ($gametype=="ordered") $flag="-o";

$var=$_GET["players"];
$names=preg_replace("/_/"," ",explode(",",$var));

if (count($names)!=4)
  error(1,"Not enough players");

for ($i=0;$i<=3;$i++) {
  $p=$names[$i];
  # Check for illigal characters
  if (preg_replace("/[^\w ]/","",$p)!=$p)
    error(1,"Player $i illegal character");
  # Check that name is not only blanks (at least 2 chars)
  if (strlen(trim($p))<2)
    error(1,"Player $i illegal name");
  # Add quotes if name includes spaces
  if (preg_match("/[ ]/",$p))
    $names[$i]="\"$p\"";
}

$args=implode(" ",$names);
$output=shell_exec("/usr/bin/python ~kristian/bordfodbold/team_predict.py $flag $args");
# Print json output from team_predict script (result or errors)
print($output."\n");

?>
